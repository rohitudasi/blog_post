<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::where('email', 'rohitudasi44@gmail.com')->get()->first();
        if (!$user) {
            //user is not created already in database so we have to make fresh user:

            User::create([
                'name' => 'rohit udasi',
                'email' => 'rohitudasi44@gmail.com',
                'password' => Hash::make('luvmylife'),
                'role' => 'admin'
            ]);
        } else {
            $user->update('role', 'admin');
        }



        User::create([
            'name' => 'mohit chhabria',
            'email' => 'mohit4@gmail.com',
            'password' => Hash::make('luvmylife'),
            'role' => 'author'
        ]);


        User::create([
            'name' => 'neeraj bathija',
            'email' => 'niraj@gmail.com',
            'password' => Hash::make('luvmylife'),
            'role' => 'author'
        ]);


        User::create([
            'name' => 'prem lalchandani',
            'email' => 'prem@gmail.com',
            'password' => Hash::make('luvmylife'),
            'role' => 'admin'
        ]);


        User::create([
            'name' => 'jayesh karia',
            'email' => 'jayesh@gmail.com',
            'password' => Hash::make('luvmylife'),
            'role' => 'author'
        ]);
    }
}
