<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Post;
use App\Tag;


class postSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //for creating post we have to first create some dummy categories and tags:

        //creating dummy categories:

        $categoryDesign = Category::create([
            'name' => 'Design'
        ]);
        $categoryCoding = Category::create([
            'name' => 'coding'
        ]);
        $categoryTechnology = Category::create([
            'name' => 'Technology'
        ]);
        $categoryNews = Category::create([
            'name' => 'News'
        ]);



        //creating dummy tags:

        $tagCustomer = Tag::create(['name' => 'customers']);
        $tagCode = Tag::create(['name' => 'code']);
        $tagprogrammer = Tag::create(['name' => 'programmer']);
        $tagDeveloper = Tag::create(['name' => 'developer']);


        //creating dummy posts:
        $post1 = Post::create([
            'title' => 'We relocated our office to HOME!',
            'excerpt' => Faker\Factory::create()->sentence(rand(15, 18)),
            'content' => Faker\Factory::create()->paragraph(rand(3, 5), true),
            'image' => 'posts/1.jpg',
            'category_id' => $categoryCoding->id,
            'user_id' => 1,
            'published_at' => \Carbon\Carbon::now()->format('y-m-d')
        ]);

        $post2 = Post::create([
            'title' => 'We found  Corona Vaccine!',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 18)),
            'content' => Faker\Factory::create()->paragraphs(rand(3, 5), true),
            'image' => 'posts/2.jpg',
            'category_id' => $categoryNews->id,
            'user_id' => 2,
            'published_at' => \Carbon\Carbon::now()->format('y-m-d')
        ]);

        $post3 = Post::create([
            'title' => 'We found big see!',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 18)),
            'content' => Faker\Factory::create()->paragraphs(rand(3, 5), true),
            'image' => 'posts/3.jpg',
            'category_id' => $categoryDesign->id,
            'user_id' => 3,
            'published_at' => \Carbon\Carbon::now()->format('y-m-d')
        ]);

        $post4 = Post::create([
            'title' => 'We found new Framework for java!',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 18)),
            'content' => Faker\Factory::create()->paragraphs(rand(3, 5), true),
            'image' => 'posts/4.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 2,
            'published_at' => \Carbon\Carbon::now()->format('y-m-d')
        ]);


        $post5 = Post::create([
            'title' => 'How to become successfull java Developer!',
            'excerpt' => Faker\Factory::create()->sentence(rand(10, 18)),
            'content' => Faker\Factory::create()->paragraphs(rand(3, 5), true),
            'image' => 'posts/5.jpg',
            'category_id' => $categoryTechnology->id,
            'user_id' => 2,
            'published_at' => \Carbon\Carbon::now()->format('y-m-d')
        ]);


        // attaching the posts with tags (filling the data in bridging table)

        $post1->tags()->attach([$tagprogrammer->id, $tagDeveloper->id]);
        $post2->tags()->attach([$tagCustomer->id, $tagCode->id]);
        $post3->tags()->attach([$tagDeveloper->id]);
        $post4->tags()->attach([$tagCode->id, $tagCustomer->id]);
        $post5->tags()->attach([$tagDeveloper->id, $tagDeveloper->id]);
    }
}
