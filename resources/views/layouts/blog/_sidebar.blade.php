
<div class="col-lg-3 sidebar pl-5 pl-lg-5 ftco-animate">
    <div class="sidebar-box">
      <form action="#" class="search-form">
        <div class="form-group">
          <span class="icon icon-search"></span>
          <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
        </div>
      </form>
    </div>
    <div class="sidebar-box ftco-animate">
      <div class="categories">
        <h3>Categories</h3>
        @foreach($categories as $category)
      <li><a href="{{route('blogs.category',$category)}}">{{$category->name }} <span class="badge badge-warning text-white">{{$category->posts()->count()}}</span>

            @endforeach
      </div>
    </div>


    <div class="sidebar-box ftco-animate">
      <h3>Tag Cloud</h3>

        @foreach($tags as $tag)
    <a href="{{route('blogs.tag',$tag)}}" class="font-weight-light p-2 mt-1 text-white badge badge-warning">{{$tag->name}}</a>
        @endforeach
      </div>
    </div>
