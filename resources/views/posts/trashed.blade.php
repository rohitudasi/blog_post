@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-end mb-3">
    <a href="{{ route('posts.create') }}" class="btn btn-primary">Add Posts</a>
</div>

<div class="card">
    <div class="card-header">Trashed Posts</div>
    <div class="card-body">
        {{-- Table to show is to be created here! --}}
        @if(!$posts->count() > 0)
            <h5>Nothing to show!!</h5>
         @else
            <table class="table tale-bordered" style="word-wrap: break-word">
                <thead>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td><img src="{{asset('storage/'.$post->image)}}" alt="Post Image" width="200"></td>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">{{$post->title}}</td>
                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">{{$post->excerpt}}</td>

                            <td style="word-wrap: break-word;min-width: 160px;max-width: 160px;">{{$post->content}}</td>
                            <td>
                                <form action="{{route('posts.restore',$post->id)}}" method="POST">
                                @method('PUT')
                                @csrf

                                <button type="submit" class="btn btn-sm btn-primary">Restore</button>
                                </form>

                                <a href="" class="btn btn-danger btn-sm"
                                onclick="displayModalForm({{$post}})"

                                data-toggle="modal"
                                data-target="#deleteModal">Delete</a>


                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="card-footer">

        {{ $posts->links() }}
    </div>
</div>

{{-- DELETE MODAL --}}

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="" method="POST" id="deleteForm">
            @csrf
            @method('DELETE')
            <div class="modal-body">
                <p>Are you sure you want to delete Post</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
        </div>
    </div>
  </div>
{{-- END DELETE MODAL --}}
@endsection



@section('page-level-scripts')
    <script type= "text/javascript">
    // page-level-scripts goes here

    function displayModalForm($post){

        let $url = '/posts/' + $post.id;
        $('#deleteForm').attr('action',$url);
    }
    </script>
@endsection

{{-- //tp files --}}
{{-- @section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function(){
        });
        $('.select2').select2();
    </script>
@endsection

@section('page-level-styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection --}}
