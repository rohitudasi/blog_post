
@extends('layouts.app');

@section('content')

<div class="card">
    <div class="card-header">Edit Post</div>

    <div class="card-body">
        <form action="{{ route('posts.update',$post->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">

                <label for="title">Title</label>

                <input type="text"
               value = "{{ old('title',$post->title) }}"
                class="form-control @error('title') is-invalid @enderror"
                value = "{{old('title')}}"
                name="title" id="title">

                @error('title')
                    <p class="text-danger">{{$message}}</p>
                @enderror

            </div>


            <div class="form-group">

                <label for="name">Excerpt</label>

                <textarea
                name="excerpt"
                id="excerpt"
                rows="4"
                class="form-control @error('excerpt') is-invalid @enderror">{{ old('excerpt',$post->excerpt) }}"</textarea>

                @error('excerpt')
                    <p class="text-danger">{{$message}}</p>
                @enderror

            </div>


            <div class="form-group">

                <label for="content">Content</label>

            <input id="content" type="hidden" name="content" value="{{old('content',$post->content)}}">
                <trix-editor input="content" ></trix-editor>
                @error('content')
                    <p class="text-danger">{{$message}}</p>
                @enderror

            </div>

            <div class="form-group">
                <select class="form-control" name="category_id" id="category_id">

                    @foreach($categories as $category)
                     <option value="{{$category->id}}" @if($category->id == $post->category_id)  selected @endif >{{$category->name}}</option>
                    @endforeach

                </select>

                @error('category_id')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>


            <div class="form-group">

                <select class="form-control select2" name="tags[]" id="tags" multiple="multiple">


                    @foreach($tags as $tag)

                     <option value="{{$tag->id}}" {{ in_array($tag->id, $post->tags->pluck('id')->toArray())  ? 'selected' : '' }}>{{$tag->name}}</option>
                    @endforeach

                </select>

                @error('tags')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-group">

                <label for="published_at">Published At</label>

                <input type="text"
                value = "{{old('published_at',$post->published_at)}}"
                class="form-control"
                name="published_at" id="published_at">

            </div>


            <div class="form-group">
            <img src="{{asset('storage/' . $post->image)}}" alt="post-image" width=100%>
            </div>

            <div class="form-group">

                <label for="image">Image</label>

                <input type="file"
                class="form-control @error('image') is-invalid @enderror"
                name="image"
                value = "{{old('image'),$post->image}}"
                id="image">


                @error('image')
                    <p class="text-danger">{{$message}}</p>
                @enderror

            </div>


            <div class="form-group">
                <button class="btn btn-success" type="submit">Update Post</button>
            </div>
        </form>
    </div>

</div>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });



        $(document).ready(function() {
    $('.select2').select2();
});

    </script>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.0/flatpickr.min.css"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css">
@endsection


