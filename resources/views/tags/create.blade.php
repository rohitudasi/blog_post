@extends('layouts.app');

@section('content')

<div class="card">

    <div class="card-header">
        @if(isset($tag))
            Edit Tag
        @else
            Add Tag
        @endif
    </div>

    <div class="card-body">
        <form action="{{ (isset($tag)) ?  route('tags.update', $tag->id): route('tags.store') }}" method="POST">
            @csrf

            @if(isset($tag))
                @method('PUT')
            @endif

            <div class="form-group">

                <label for="name">Name</label>

                <input type="text"
                value= "{{ isset($tag) ? $tag->name : ''}}"
                class="form-control @error('name') is-invalid @enderror"
                name="name" id="name">

                @error('name')
                    <p class="text-danger">{{$message}}</p>
                @enderror

            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    Add Tag</button>
            </div>
        </form>
    </div>

</div>
@endsection
