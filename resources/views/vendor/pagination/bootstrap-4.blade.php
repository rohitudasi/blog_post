@if ($paginator->hasPages())
<div class="row mt-5">
    <div class="col text-center">
      <div class="block-27">
        <ul>


            @if ($paginator->onFirstPage())
            <li class="disabled" aria-disabled="true">
                <span class="" aria-hidden="true">&lt;</span>
            </li>
        @else
            <li>
                <a href="{{ $paginator->previousPageUrl() }}">&lt;</a>
            </li>
        @endif


                 {{-- Array Of Links --}}

                 @foreach ($elements as $element)
                 @if (is_array($element))
                 @foreach ($element as $page => $url)
                     @if ($page == $paginator->currentPage())

                     <li class="active" aria-current="page"><span class="">{{ $page }}</span></li>
                     @else
                         <li class=""><a class="" href="{{ $url }}">{{ $page }}</a></li>
                     @endif
                 @endforeach
             @endif
                @endforeach

             @if ($paginator->hasMorePages())
             <li><a href="{{ $paginator->nextPageUrl() }}">&gt;</a></li>
         @else
             <li class="disabled" aria-disabled="true">
                 <span class="" aria-hidden="true">&gt;</span>
             </li>
         @endif


        </ul>
      </div>
    </div>
  </div>


@endif
