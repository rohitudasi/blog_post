@extends('layouts.app')

@section('content')

@include('layouts._message')
<div class="d-flex justify-content-end mb-3">
    <a href="{{ route('register') }}" class="btn btn-primary">Add Users</a>
</div>

<div class="card">
    <div class="card-header">Tags</div>
    <div class="card-body">
        {{-- Table to show is to be created here! --}}

        <table class="table table-bordered">
            <thead>
                <th>Image</th>
                <th>Name</th>
                <th>Posts Count</th>
                <th>Email</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                    <td><img src="{{ Gravatar::src($user->email) }}" alt=""></td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->posts->count()}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                        @if(!$user->isAdmin())
                        <form action="{{route('users.make-admin',$user->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                            <button type="submi" class="btn btn-primary btn-sm">Make Admin</button>
                        </form>
                        @else
                            <h5>Already Admin</h5>
                        @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
