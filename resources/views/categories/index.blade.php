@extends('layouts.app')



@section('content')

@include('layouts._message')
<div class="d-flex justify-content-end mb-3">
    <a href="{{ route('categories.create') }}" class="btn btn-primary">Add Category</a>
</div>

<div class="card">
    <div class="card-header">categories</div>
    <div class="card-body">
        {{-- Table to show is to be created here! --}}

        <table class="table tale-bordered">
            <thead>
                <th>Name</th>
                <th>Actions</th>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td><a href="{{route('categories.edit',$category->id)}}" class="btn btn-warning btn-sm">Edit</a>
                        <a href="#" onclick="displayModalForm({{$category}})"
                         class="btn btn-danger btn-sm"

                            data-toggle="modal"
                            data-target="#deleteModal">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

{{-- DELETE MODAL --}}

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="" method="POST" id="deleteForm">
            @csrf
            @method('DELETE')
            <div class="modal-body">
                <p>Are you sure you want to delete category</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
        </div>
    </div>
  </div>
{{-- END DELETE MODAL --}}
@endsection



@section('page-level-scripts')
  <script type= "text/javascript">


    function hel(){
        console.log('hel');
    }
    function displayModalForm($category)
    {
        var url = '/categories/' + $category.id;
        $('#deleteForm').attr('action',url);
    }
</script>
@endsection
