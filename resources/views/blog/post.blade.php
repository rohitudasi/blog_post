@extends('layouts.blog.layout')


@section('title','single-post')

@section('header')

<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url(asset('assets/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
        <div class="col-md-9 ftco-animate pb-5 text-center">
          <h1 class="mb-3 bread">Blog Single</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a href="blog.html">Blog <i class="ion-ios-arrow-forward"></i></a></span> <span>Blog Single <i class="ion-ios-arrow-forward"></i></span></p>
        </div>
      </div>
    </div>
  </section>
@endsection


@section('main-content')

<div class="col-lg-8 ftco-animate">

    <h2 class="mb-4">{{$post->title}}</h2>

    <div class="post-info text-secondary mb-3">
        <span class="icon-pencil text-capitalize"> {{$post->published_at->diffForHumans()}}<span class="text-warning">   |   </span></span>
        <span class="icon-user text-capitalize"> {{$post->author->name}}<span class="text-warning">   |   </span></span>
        <span class="icon-thumbs-up"> 90</span>
        </div>
    <div class="post-image-container">
        <img src="{{asset('storage/' . $post->image )}}" alt="" class="img-fluid">
    </div>

  {!! $post->content !!}
    <div class="tag-widget post-tag-container mb-5 mt-5">
    <div class="tagcloud">
        @foreach($post->tags as $tag)
    <a href="{{route('blogs.tag',$tag->id)}}" class="tag-cloud-link">{{$tag->name}}</a>
        @endforeach
    </div>
  </div>

  <div class="about-author d-flex p-4 mb-5 bg-light">
    <div class="bio mr-5">
    <img src="{{Gravatar::src($post->author->email)}}" alt="Image placeholder" class="img-fluid mb-4">
    </div>
    <div class="desc">
      <h3>{{$post->author->name}}</h3>
      <p>{{$post->author->about ?? "no description"}}</p>
    </div>
  </div>

  <div id="disqus_thread"></div>
<script>

 var disqus_config = function () {
 this.page.url = "{{ route('blogs.post',$post->id) }}";  // Replace PAGE_URL with your page's canonical URL variable
 this.page.identifier = {{ $post->id }}; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://blog-post-3.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


</div>

@endsection


