<?php

namespace App\Http\Requests\posts;

use Illuminate\Foundation\Http\FormRequest;

class CreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // auth(): returns available auth instance
        // check(): Determines current user is authenticed or not
        return auth()->check(); //if it returns false request does not fulfill further.
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|unique:posts',
            'excerpt' => 'required|max:255',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,gif,svg,jpg,|max:1024',
            'category_id' => 'required|exists:categories,id',


        ];
    }
}
