<?php

namespace App\Http\Middleware;

use Closure;
use App\Post;

class VerifyAuthorForEditAndDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (is_numeric($request->post)) {
            //this means trash function is called and because post is already deleted we donot able to get object of Post as model route binding is not enabled.(we only get id of that post instead of object of post)
            $post_id = $request->post;
            $post = Post::onlyTrashed()->findOrFail($post_id);
        } else {
            $post = $request->post;
        }

        //check that user is authenticated or not to make actions:

        if (!($post->user_id === auth()->id())) {
            //This means the author of the post and logged in user is not same so we have to redirect to the 401 page
            return redirect(abort(401));
        }

        return $next($request);
    }
}
