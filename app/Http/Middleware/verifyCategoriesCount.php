<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;

class verifyCategoriesCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(Category::all()->count() === 0) {
        if (Category::count() === 0) {

            session()->flash('error', 'Minimum one category must exist to create a post');
            return redirect(route('categories.create'));
        }

        return $next($request);

        //next thing is to register this middleware
    }
}
