<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class usersController extends Controller
{

    //

    public function index()
    {

        return view('users.index', ['users' => User::all()]);
    }

    public function makeAdmin(USer $user)
    {
        $user->update(['role' => 'admin']);
        session()->flash('succes', $user->name . 'has been assigned Admin role');
        return redirect(route('users.index'));
    }
}
