<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\posts\CreatePostRequest;
use App\Http\Requests\posts\UpdatePostRequest;
use App\Category;
use App\Tag;

class postsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['verifyCategoriesCount'])->only('create', 'store');
        $this->middleware(['verifyAuthor'])->only('edit', 'update', 'destroy', 'trash');
    }
    public function index()
    {
        //

        if (!auth()->user()->isAdmin()) {
            $posts = Post::withoutTrashed()->where('user_id', auth()->id())->paginate(10);
        } else {
            $posts = Post::paginate(10);
        }
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {

        //image upload and stores the name of the image:
        $image = $request->file('image')->store('posts'); //this line will result in to store the images in to storage folder inside storage
        //run the command :php artisan storage:link


        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'content' => $request->content,
            'image' => $image,
            'user_id' => auth()->id(),
            'category_id' => $request->category_id,
            'published_at' => now() //TODO have to set timezone.

        ]);

        $post->tags()->attach($request->tags);
        session()->flash('success', 'Post has been successfully created!');
        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        //
        $data = $request->only(['title', 'excerpt', 'content', 'published_at', 'category_id']);


        if ($request->hasFile('image')) {
            //store the image and delete the old image:
            $image = $request->image->store('posts');
            $post->deleteImage();
            $data['image'] = $image;
        }

        $post->update($data);

        $post->tags()->sync($request->tags);
        session()->flash('success', 'Post has been updated successfully!');
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //ider post ka object seeda nhi mil payaga q ke soft delete hai na voh toh humko iske id se iska object lana padaga iske model binding nhi ho payage:
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
        session()->flash('success', 'Post Deleted Successfully!');
        return redirect()->back(); //Create a new redirect response to the previous location.
    }

    // soft deletes the posts
    public function trash(Post $post)
    {
        //TODO if post will soft delete then also category will not be deletd
        $post->delete(); //This will do soft-delete
        session()->flash('success', 'Post trashed successfully!');
        return redirect(route('posts.index'));
    }

    // function for showing all the trashed(soft-deleted) posts.
    public function trashed()
    {
        // $trashed = DB::table('posts')->whereNotNull('deleted_at');
        $trashed = Post::onlyTrashed()->paginate(10);
        return view('posts.trashed')->with('posts', $trashed); //with: adds a piece of data to the view
    }

    // function to restore the post
    public function restore($id)
    {
        $trashedPost = Post::onlyTrashed()->findOrFail($id);
        $trashedPost->restore();
        session()->flash('success', "Post has been restored successfully!");
        return redirect(route('posts.index'));
    }
}
