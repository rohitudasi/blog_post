<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    //
    public function index()
    {

        return view('blog.index', [
            'posts' => Post::paginate(9),
            'categories' => Category::all(),
            'tags' => Tag::all(),
        ]);
    }

    public function category(Category $category)
    {
        $category = Category::findOrFail($category->id);

        return view('blog.index', [
            'posts' => $category->posts()->paginate(9),
            'categories' => Category::all(),
            'tags' => Tag::all(),
        ]);
    }

    public function tag(Tag $tag)
    {
        $tag = Tag::findOrFail($tag->id);

        return view('blog.index', [
            'posts' => $tag->posts()->paginate(9),
            'categories' => Category::all(),
            'tags' => Tag::all(),
        ]);
    }

    public function post(Post $post)
    {

        $post = Post::findOrFail($post->id);
        return view('blog.post', [
            'posts' => Post::all(),
            'categories' => Category::all(),
            'tags' => Tag::all(),
            'post' => $post,
        ]);
    }
}
