<?php

namespace App;

use App\Http\Middleware\verifyCategoriesCount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{


    use SoftDeletes; //it is a trait which add additional methods related to soft delete such as delete,forceDelete and restore:
    //fillable for maas assigment:

    protected $dates = ['published_at'];
    protected $fillable =  [
        'title',
        'excerpt',
        'user_id',
        'content',
        'image',
        'category_id',
        'published_at'
    ];

    // This function is used to delete the file(image) from local storage during delete and updation of post.
    public function deleteImage()
    {
        Storage::delete($this->image);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id'); //second argument is to specify the foreign key as we have changed the name of the function fri user to author:
    }
}
