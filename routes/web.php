<?php

use App\Http\Controllers\categoriesController;
use App\Http\Controllers\postsController;
use App\Http\Controllers\usersController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/blog/category/{category}', 'WelcomeController@category')->name('blogs.category');
Route::get('/blog/tag/{tag}', 'WelcomeController@tag')->name('blogs.tag');
Route::get('/blog/post/{post}', 'WelcomeController@post')->name('blogs.post');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('categories', 'categoriesController');
    Route::resource('posts', 'postsController');
    Route::resource('tags', 'tagsController');

    // Route for soft delete:

    Route::delete('/trash/{post}', 'postsController@trash')->name('posts.trash');

    // index of trashes posts

    Route::get('/trashed', 'postsController@trashed')->name('posts.trashed');

    // restore

    Route::put('/restore/{post}', 'postsController@restore')->name('posts.restore');
});


Route::middleware(['auth', 'verifyAdmin'])->group(function () {
    Route::get('/users', 'usersController@index')->name('users.index');
    Route::put('/users/{user}/make-admin', 'usersController@makeAdmin')->name('users.make-admin');
});
